package at.kriegl.searcher;


import at.kriegl.helper.DataGenerator;

public class SequentialSearcher {
	
	public static void main(String[] args) {
		
			search(17);
		
	}

	
	public static int search (int number){
		int result = 0;
		int[] array = DataGenerator.generateRandomData(10, 30);
		
		for (int i = 0; i < array.length; i++) {
			if (number == array[i]){
				result = i +1;
				System.out.print("Die gesuchte Zahl befindet sich an " + result + ". Stelle.");
				}
			else if ( i == array.length-1){
				System.out.println("--Ende--");
			}
	
		}
		return result;

	}
	
}
