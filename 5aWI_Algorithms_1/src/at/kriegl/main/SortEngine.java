package at.kriegl.main;
import at.kriegl.sort_algorithms.SortAlgorithm;;

public class SortEngine {
	
	private SortAlgorithm sortAlgorithm;
	
	public int[] sort(int[] data){
		return sortAlgorithm.sort(data);
		
	}
	
	public void setAlgorithm(SortAlgorithm algo){
		this.sortAlgorithm = algo;
	}
	
}
