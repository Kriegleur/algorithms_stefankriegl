package at.kriegl.main;
import at.kriegl.helper.DataGenerator;
import at.kriegl.sort_algorithms.InsertionSorter;
import at.kriegl.sort_algorithms.SortAlgorithm;

public class Application {

	public static void main(String[] args) {
		
		SortAlgorithm algo = new InsertionSorter();
		int[] arr1 = DataGenerator.generateRandomData(30, 50);
		SortEngine se = new SortEngine();
		se.setAlgorithm(algo);
		DataGenerator.printData(se.sort(arr1));
		
		
	}
		
}
