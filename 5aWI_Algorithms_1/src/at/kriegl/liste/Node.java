package at.kriegl.liste;

public class Node {

	public int value;
	  public int next;
	  public int previous;
	  
	  public Node(int value) {
	    super();
	    this.value = value;
	  }
	  public int getValue() {
	    return value;
	  }
	  public void setValue(int value) {
	    this.value = value;
	  }
	  public int getNext() {
	    return next;
	  }
	  public void setNext(int next) {
	    this.next = next;
	  }
	  public int getPrevious() {
	    return previous;
	  }
	  public void setPrevious(int previous) {
	    this.previous = previous;
	  }

}
