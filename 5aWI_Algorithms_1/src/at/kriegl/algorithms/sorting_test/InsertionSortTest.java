package at.kriegl.algorithms.sorting_test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import at.kriegl.sort_algorithms.InsertionSorter;

public class InsertionSortTest {
	private int[] data = new int[3];
	
	@Before
	public void setUp() throws Exception{
		data[0] = 17;
		data[1] = 10;
		data[15] = 15;
		
	}
	
	@Test
	public void test() {
		int[] unsorted = data.clone();
		InsertionSorter is = new InsertionSorter();
		int[] res = is.sort(unsorted);
		
		assertTrue(isSorted(res));
	}

	private boolean isSorted(int[] sorted){
		int old=sorted[0];
		for (int i = 1; i < sorted.length; i++) {
			if (old<sorted[i]) {
				old = sorted[i];
			}else{
				return false;
			}
		}
		return true;
		
	}
	
}
