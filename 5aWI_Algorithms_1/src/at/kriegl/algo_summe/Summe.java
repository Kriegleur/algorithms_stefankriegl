package at.kriegl.algo_summe;


public class Summe {
	
	
	public static void main(String[] args) {
		summe(16,26);
	}

	
	public static int summe(int Zahl1, int Zahl2) {
	    int result = 0;
	    
	    String StringZahl1 = Integer.toString(Zahl1);
	      String StringZahl2 = Integer.toString(Zahl2);
	      
	      char[] x = StringZahl1.toCharArray();
	      char[] y = StringZahl2.toCharArray();
	      
	      int ZehnerStelle = Integer.parseInt(String.valueOf(x[0])) + Integer.parseInt(String.valueOf(y[0]));
	      int EinserStelle = Integer.parseInt(String.valueOf(x[1])) + Integer.parseInt(String.valueOf(y[1]));
	      
	      result = ZehnerStelle*10 + EinserStelle;
	    
	      System.out.println(result);
	      
	      return result;
	  }
	  
	
}
