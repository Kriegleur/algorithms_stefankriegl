package at.kriegl.sort_algorithms;


public class SelectionSorter implements SortAlgorithm {

     
//public static void main(String a[]){
//         
//        int[] arr1 = DataGenerator.generateRandomData(30, 50);;
//        int[] arr2 = doSelectionSort(arr1);
//        for(int i:arr2){
// 
//            System.out.print(i);
//            System.out.print(", ");
//            
//        }
//}

	@Override
	public int[] sort(int[] arr) {
		  for (int i = 0; i < arr.length - 1; i++)
	        {
	            int index = i;
	            for (int j = i + 1; j < arr.length; j++)
	                if (arr[j] < arr[index])
	                    index = j;
	      
	            int smallerNumber = arr[index]; 
	            arr[index] = arr[i];
	            arr[i] = smallerNumber;
	        }
	        return arr;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "SelectionSorter";
	}
}

