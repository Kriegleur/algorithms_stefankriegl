package at.kriegl.sort_algorithms;

public interface SortAlgorithm {
	public int[] sort(int[] data);
	
	public String getName();
	
}
