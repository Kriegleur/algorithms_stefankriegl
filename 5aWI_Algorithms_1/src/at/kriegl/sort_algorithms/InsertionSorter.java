package at.kriegl.sort_algorithms;


public class InsertionSorter implements SortAlgorithm {
	  

//Insertion
  
	@Override
	public int[] sort(int[] input) {
	
          for (int i = 1; i < input.length; i++) {
              for(int j = i ; j > 0 ; j--){
                  if(input[j] < input[j-1]){
                      int a = input[j];
                      input[j] = input[j-1];
                      input[j-1] = a;
                  }
              }
          }
          return input;
          

		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		
		return "InsertionSorter";
		
	}
      
      
}
