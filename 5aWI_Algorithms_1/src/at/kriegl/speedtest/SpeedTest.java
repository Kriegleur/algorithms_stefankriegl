package at.kriegl.speedtest;

import java.util.ArrayList;
import java.util.List;
import at.kriegl.helper.*;
import at.kriegl.sort_algorithms.BubbleSorter;
import at.kriegl.sort_algorithms.InsertionSorter;
import at.kriegl.sort_algorithms.SelectionSorter;
import at.kriegl.sort_algorithms.SortAlgorithm;


public class SpeedTest {

    private List<SortAlgorithm> algorithms;
    
    public SpeedTest() {
      this.algorithms = new ArrayList<>();
      addAlgorithm(new InsertionSorter());
      addAlgorithm(new SelectionSorter());
      addAlgorithm(new BubbleSorter());
    }
    
    public void addAlgorithm(SortAlgorithm algo) {
      this.algorithms.add(algo);
    }
    
    public void run() {
      int[] data = DataGenerator.generateRandomData(10000, 17);
      
      for (SortAlgorithm s : algorithms) {
        System.out.println(s.getName());
        
        final long timeStart = System.currentTimeMillis(); 
        s.sort(data);
        final long timeEnd = System.currentTimeMillis(); 
        System.out.println("Delta Time " + (timeEnd - timeStart) + " Millisek.");
      }
    }
    
    public static void main(String[] args) {
      SpeedTest st = new SpeedTest();
      st.run();
    }
  }