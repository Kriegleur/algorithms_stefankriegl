package at.kriegl.helper;


import java.util.Random;

public class DataGenerator {


	
	public static int[] generateRandomData (int size, int maxValue){
		Random random = new Random();
		
		
		int[] array = new int[size];
		
		for (int i = 0; i < size; i++) {
			
			array[i] = random.nextInt(maxValue);
			
		}
		
		System.out.println(array);
		
		return array;

		
	}
	
	public static void printData(int[] array1){
		
		for (int i : array1) {
			System.out.println(array1[i]);
		}
	}
	
	

}
